# vim: set et ts=4 foldmethod=marker foldmarker={{{,}}} :

die() {
    echo -n "FATAL: "
    echo "${@}"
    sremove;
    sumount;
    exit 1;
}

smount() {
    [[ -d ${1} ]] || die "Cannot bind mount ${1} as it is not a dir";
    [[ -d ${2} ]] || die "Cannot mount over ${2} as it is not a dir. Make sure to create it with correct permissions.";
    echo -n "edo ('${TARGET}, $(date -u)'): "
    mount -v --bind "${1}" "${2}" || die "Mounting ${2} failed";
    MOUNTED+=( "${2}" );
}

sumount() {
    if [[ -n ${MOUNTED[@]} ]]; then
        local i error=();
        for((i=${#MOUNTED[@]}-1; i>=0; i--)); do
            umount -v "${MOUNTED[i]}" || error+=("${MOUNTED[i]}");
        done;
        [[ -n ${error[@]} ]] && echo "Failed to umount: ${error[*]}" && exit 1;
    fi;
}

scopy() {
    cp "${1}" "${2}";
    COPIED+=( "${2}" );
}

sremove() {
    if [[ -n ${COPIED[@]} ]]; then
        local i
        for((i=${#COPIED[@]}-1; i>=0; i--)); do
            rm "${COPIED[i]}";
        done;
    fi;
}

edo() {
    echo "edo ('${TARGET}, $(date -u)'): ${@}";
    "${@}" || die "${@} failed${DIE:+, ${DIE}}";
}
