
# show_file_contents <file>
#
# Prints the contents of a file
#
show_file_contents() {
    local file=${1}
    local basename=$(basename ${1})

    echo -e "\e[35m${basename}:\e[0m"
    echo -e "\e[44m%<-----\e[0m"
    cat ${file}
    echo -e "\e[44m>%-----\e[0m"
    echo ""
}

# show_config <config directory>
#
# Print the contents of all files in a paludis config directory
#
show_config() {
    local config_dir=${1}

    for file in $(find ${config_dir} -type f); do
        show_file_contents ${file}
    done
}

