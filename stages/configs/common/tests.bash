CPN_SKIP_TESTS=(
    ### Disabled tests to avoid packages

    # pulls in packages from ::gnome and ::x11
    # last checked: 28 Jan 2019 (version )
    dev-libs/glib

    # pulls in packages from ::gnome, ::x11 and ::desktop
    # last checked: 28 Jan 2019 (version )
    gnome-desktop/dconf

    # pulls in a number of packages from ::perl
    # last checked: 20121114
    net-misc/wget

    # needs Data-Compare, Data-Dump and Test-Deep from ::perl
    # last checked: 28 Jan 2019 (version )
    sys-apps/texinfo

    # pulls in libgfortran
    sys-devel/libtool

    # pulls in stuff from ::perl for tests
    dev-scm/git


    ### Tests that don't work in CI environment
    app-editors/vim


    ### Misc failures
    ## Broken

    # Tries to use `gcc` - probably dejagnu related
    # Last checked: 01 May 2020 (version 3.2.1-r1)
    # Context:
    # Running ../../testsuite/libffi.call/call.exp ...
    # ERROR: tcl error sourcing ../../testsuite/libffi.call/call.exp.
    # ERROR: !!! Exheres bug: 'gcc' banned by distribution
    dev-libs/libffi


    ## Test failures

    # Tests hang / wait for input
    # Last checked: 05 Mar 2019 (version: 7.64.0)
    net-misc/curl

    # Fails some tests within containers
    # Last checked: 30 Mar 2019 (version: 241-r2)
    sys-apps/systemd

    # Fails two tests
    # Last checked: 29 Apr 2020 (version: 5.28.2)
    # Context:
    # Test Summary Report
    # -------------------
    # ../cpan/Time-Local/t/Local.t             (Wstat: 512 Tests: 187 Failed: 2)
    #   Failed tests:  6, 12
    #   Non-zero exit status: 2
    dev-lang/perl

    # Testcase seems broken
    # Last checked: 02 May 2020 (version: 4.8)
    # Context:
    # inplace-selinux.sh: set-up failure: CONFIG_HEADER not defined
    # ERROR: testsuite/inplace-selinux.sh
    sys-apps/sed

    # Fails some tests on at least x86_64 gnu/musl
    # Last checked: 02 May 2020 (version 2.35.1)
    # Context:
    #         ipcs: mk-rm-sem                                     ... FAILED (ipcs/mk-rm-sem)
    #         ipcs: mk-rm-shm                                     ... FAILED (ipcs/mk-rm-shm)
    #         ipcs: mk-rm-msg                                     ... FAILED (ipcs/mk-rm-msg)
    #
    # On musl the same tests fail plus the following:
    #       getopt: options: [28] posix_correctly                 ... FAILED (getopt/options-posix_correctly)
    sys-apps/util-linux

    # connection-gnutls test fails at on x86_64 gnu/musl
    # Last checked: 03 May 2020 (version 2.62.3)
    # Context:
    # Bail out! GLib-Net:ERROR:../glib-networking-2.62.3/tls/tests/connection.c:2035:quit_on_handshake_complete: assertion failed (error == (g-tls-error-quark, 3)): error is NULL
    dev-libs/glib-networking
)
