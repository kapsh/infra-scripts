CPN_SKIP_TESTS+=(
    # Fails some tests
    # Last checked: 10 May 2020 (version 1.45.6)
    # Context:
    # 346 tests succeeded	10 tests failed
    # Tests failed: d_loaddump f_bigalloc_badinode f_bigalloc_orphan_list f_dup4 f_dup_resize m_minrootdir m_rootdir r_1024_small_bg r_bigalloc_big_expand r_ext4_small_bg
    #
    # The same version passed all its tests before, the tests started failing
    # after a kernel update to 5.7.0-rc3
    sys-fs/e2fsprogs

    # Fails two tests
    # Last checked: 01 May 2020 (version 1.4.18-r1)
    # Context:
    # ../build-aux/test-driver: line 107:  5038 Aborted                 (core dumped) "$@" > $log_file 2>&1
    # FAIL: test-localename
    # FAIL: test-mbrtowc5.sh
    sys-devel/m4

    # Fails some tests
    # Last checked: 02 May 2020 (version 2.9.10-r1)
    # Context:
    # ## XML regression tests
    # File ./test/icu_parse_test.xml generated an error
    # ## XML regression tests on memory
    # File ./test/icu_parse_test.xml generated an error
    # ## XML entity subst regression tests
    # File ./test/icu_parse_test.xml generated an error
    #
    # [ more icu_parse_test.xml related errors ]
    #
    # Total 3174 tests, 11 errors, 0 leaks
    dev-libs/libxml2

    # Fails some tests
    # Last checked: 02 May 2020 (version 0.20.2)
    # Context:
    # FAIL: msgconv-2
    # FAIL: msgmerge-compendium-6
    # FAIL: xgettext-python-3
    sys-devel/gettext

    # Fails a test
    # Last checked: 02 May 2020 (version 2.4.48)
    # Context:
    # [13] $ setfattr -n user -v value f -- failed
    # setfattr: f: Not supported                                               != setfattr: f: Operation not supported
    #
    # Problem:
    # musl stringifies errno ENOTSUP differently than glibc and attr's test
    # hardcodes that error string
    sys-apps/attr

    # Fails some tests
    # Last checked: 03 May 2020 (version: 5.1.0)
    # Context:
    # 3 TESTS FAILED
    # _clos1way6: (wrong output order)
    # _nonfatal3:
    #   nonfatal3.awk:4: warning: remote host and port information (localhost, 0) invalid: System error
    # _testext:
    #   ! test_errno() returned 1, ERRNO = No child processes
    #   vs.
    #   ! test_errno() returned 1, ERRNO = No child process
    sys-apps/gawk

    # Fails some tests
    # Last checked: 11 May 2020 (version 1.31.1)
    # Context:
    # FAIL: cpio uses by default uid/gid
    # FAIL: cpio -R with create
    # FAIL: cpio -R with extract
    #
    # All errors are of the following kind:
    #
    # --- expected
    # +++ actual
    # @@ -1,2 +1,2 @@
    # -103/443
    # +blocks
    #  0
    #
    # FAIL: patch detects already applied hunk
    # FAIL: patch detects already applied hunk at the EOF
    #
    # Both errors are of the following kind:
    #
    # --- expected
    # +++ actual
    # @@ -1,9 +1,9 @@
    # +patching file input
    #  Possibly reversed hunk 1 at 4
    #  Hunk 1 FAILED 1/1.
    #   abc
    #  +def
    #   123
    # -patching file input
    #  1
    #  abc
    #  def
    #
    sys-apps/busybox

    # Fails some tests
    # Last checked: 11 May 2020 (version 3.7.7)
    # Context:
    # 6 tests failed again:
    #     test__locale test_c_locale_coercion test_locale test_re
    #     test_strptime test_time
    dev-lang/python:3.7

    # Fails a test
    # last checked: 12 May 2020 (version: 10.35)
    # Context:
    # FAIL: RunGrepTest
    #
    #  @@ -1,22 +1,22 @@
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    # +The quick brown
    #  Arg1: [T] [his] [s] Arg2: |T| () () (0)
    #  Arg1: [T] [his] [s] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    # -The quick brown
    #  [...]
    dev-libs/pcre2

    # Fails a test
    # Last checked: 12 May 2020 (version 1.8.31_p1)
    # Context:
    # strsig_test: 84 tests run, 1 errors, 98% success rate
    # strsig_test: FAIL: str2sig(SIGSYS): -1 != 0
    app-admin/sudo
)
