CPN_SKIP_TESTS+=(
    # Fails many tests when updating from 3.5.19 (because its using the old system lib?)
    # Last checked: 15 Aug 2019 (version: 3.6.8)
    # Context:
    #
    # ============================================================================
    # Testsuite summary for GnuTLS 3.6.8
    # ============================================================================
    # # TOTAL: 365
    # # PASS:  13
    # # SKIP:  8
    # # XFAIL: 0
    # # FAIL:  344
    # # XPASS: 0
    # # ERROR: 0
    #
    dev-libs/gnutls

    # Some tests timeout
    # Last checked: 18 Aug 2019 (version: 1.4.2)
    # Context:
    #
    # ninja -v -j2 test
    #  1/12 test-zstd                               TIMEOUT 2800.64 s
    #  2/12 test-fullbench-1                        TIMEOUT 84.01 s
    #  3/12 test-fullbench-2                        OK      20.27 s
    #  4/12 test-zbuff                              TIMEOUT 129.30 s
    #  5/12 test-zstream-1                          TIMEOUT 240.08 s
    #  6/12 test-zstream-2                          OK       0.36 s
    #  7/12 test-zstream-3                          TIMEOUT 120.13 s
    #  8/12 test-longmatch                          TIMEOUT 63.08 s
    #  9/12 test-invalidDictionaries                OK       0.08 s
    # 10/12 test-symbols                            OK       0.18 s
    # 11/12 test-decodecorpus                       OK      31.57 s
    # 12/12 test-poolTests                          OK       0.81 s
    #
    # Ok:                    6
    # Expected Fail:         0
    # Fail:                  0
    # Unexpected Pass:       0
    # Skipped:               0
    # Timeout:               6
    app-arch/zstd

    # 2 tests fail with old stage (but pass on newer armv7 systems outside docker)
    # Last checked: 10 Sep 2019 (version: 1.4.18-r1)
    # Context:
    #
    # ============================================================================
    # Testsuite summary for GNU M4 1.4.18
    # ============================================================================
    # # TOTAL: 170
    # # PASS:  148
    # # SKIP:  20
    # # XFAIL: 0
    # # FAIL:  2
    # # XPASS: 0
    # # ERROR: 0
    #
    # FAIL: test-dup2
    # ../build-aux/test-driver: line 107: 18645 Aborted                 (core dumped) "$@" > $log_file 2>&1
    # FAIL: test-getdtablesize
    # ../build-aux/test-driver: line 107: 18899 Aborted                 (core dumped) "$@" > $log_file 2>&1
    sys-devel/m4

    # Testsuite aborts with error
    # Last checked: 13 Sep 2019 (version 6.0-r5)
    # Context:
    # #####  testing unzip -o and funzip (ignore funzip warning)
    # funzip warning: zipfile has more than one entry--rest ignored
    # error: invalid zip file with overlapped components (possible zip bomb)
    # make: *** [Makefile:501: check] Error 12
    app-arch/unzip

    # Fails two tests
    # Last checked: 07 May 2020 (version 1.0.7)
    # Context:
    # FAIL: t0004-core-chdir.sh
    # FAIL: t0003-core-basic.sh
    sys-apps/sydbox

    # Fails a test
    # Last checked: 08 Sep 2020 (version: 1.10.0-r1)
    dev-cpp/gtest

)
