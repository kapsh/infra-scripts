#!/bin/bash

if [[ -z "$1" ]] ; then
    echo "USAGE: $0 stage_archive"
    exit 1
fi

ARCHIVE=$1
NAME=$(basename "${ARCHIVE}")
NAME=${NAME%.tar.xz}

set -e

echo "Importing base container..."
docker import "${ARCHIVE}"  exherbo/${NAME}-base
docker push exherbo/${NAME}-base

if [[ ${NAME} == "exherbo-x86_64-pc-linux-gnu" ]] ; then
    echo "Building CI container..."
    pushd "$(dirname "${BASH_SOURCE[0]}")"/ci
    docker build --no-cache --pull -t exherbo/exherbo_ci .
    docker push exherbo/exherbo_ci
    popd

    echo "Building libreSSL CI container..."
    pushd "$(dirname "${BASH_SOURCE[0]}")"/ci
    docker build --no-cache --pull --build-arg="sslprovider=libressl" -t exherbo/exherbo_ci:libressl .
    docker push exherbo/exherbo_ci:libressl
    popd

    echo "Building DID container..."
    pushd "$(dirname "${BASH_SOURCE[0]}")"/did
    docker build --no-cache --pull -t exherbo/exherbo_did .
    docker push exherbo/exherbo_did
    popd
fi

